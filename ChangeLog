2024-08-08  Bastian Germann <bastiangermann@fishpost.de>

	* src/unrar.c: Explicitly cast pointer

2024-03-09  Bastian Germann <bastiangermann@fishpost.de>

	* unrar-free.1: Correct print command in man
	* README: Remove unnecessary documentation

2024-03-08  Bastian Germann <bastiangermann@fishpost.de>

	* src/unrar.c, src/opts.h: Add multi-volume detection

2024-03-03  Bastian Germann <bastiangermann@fishpost.de>

	* src/unrar.c, src/opts.*: Add outfile option

2024-03-01  Bastian Germann <bastiangermann@fishpost.de>

	* src/unrar.c, src/opts.*: Add print command

2024-01-13  Bastian Germann <bastiangermann@fishpost.de>

	* src/unrar.c, src/opts.*: Move from glibc error() to own implementation

2022-12-04  Ying-Chun Liu (PaulLiu) <paulliu@debian.org>

	* misc/tarar.pike: use our own crc32 implementation

2022-10-20  Bastian Germann <bastiangermann@fishpost.de>

	* src/unrar.c: Do not output 'All OK' when it isn't

2021-11-06  Ying-Chun Liu (PaulLiu) <paulliu@debian.org>

	* src/unrar.c: hardening
	This commit patches some hardening errors when building in Debian.

2021-10-29  Bastian Germann  <bastiangermann@fishpost.de>

	* unrar.c: Fix one -Wmisleading-indentation
	* unrar.c: Fix format specifiers

2021-09-25  Bastian Germann  <bastiangermann@fishpost.de>

	* src/unrar.c: Reimplement based on libarchive (Adds RAR 5.0 support)
	* src/unrar.c: Get rid of Info-ZIP code and license
	* src/Makefile.am: Remove unrarlib from the project
	* configure.ac: Rename executable to unrar-free by default
	* configure.ac: Update/remove deprecated AC macros
	* unrar-free.1: Remove outdated BUGS section

2021-09-21  Bastian Germann  <bastiangermann@fishpost.de>

	* unrar-free.1: Add man page by Niklas Vainio
	* src/unrar.c: Remove unar code, which drops RARv3 compatibility
	* src/opts.c: Add ineffective -y for compatibility
	* src/unrar.c: Add missing Info-ZIP license
	* src/unrar.c: Fix CVE-2017-14120
	* src/unrarlib.c: Fix CVE-2017-11190, CVE-2017-11189/CVE-2017-14121, and CVE-2017-14122

2014-07-07  Ying-Chun Liu  <grandpaul@gmail.com>

	* src/unrar.c: Use unar to handle RARv3 format when it is installed

2007-11-27  Ying-Chun Liu  <grandpaul@gmail.com>

	* src: Remove RARv3 code from libclamav due to unclear license

2007-05-15  Ying-Chun Liu  <grandpaul@gmail.com>

	* src/unrar.c: mkpath if path does not exist when extracting files
	* misc/tarar.pike: Add new pike program

2007-04-24  Ying-Chun Liu  <grandpaul@gmail.com>

	* src/unrarlib.c: Handle unknown header info
	* src: Merge RARv3 from unrarlib SVN, which comes from libclamav

2006-06-09  Ying-Chun Liu  <grandpaul@gmail.com>

	* src/unrarlib.c: Fix warnings: dereferencing type-punned pointer

2006-04-30  Ying-Chun Liu  <grandpaul@gmail.com>

	* src/unrar.c: Make parent of directories if it doesn't exist
	* src/unrarlib.c: Fix the type of the return value of CalcCRC32()

2006-04-12  Ying-Chun Liu  <grandpaul@gmail.com>

	* src/opts.c: Avoid extracting with non-free unrar command "t"

2006-03-05  Ying-Chun Liu  <grandpaul@gmail.com>

	* src/unrarlib.c: Remove filename length 260 bytes restriction
	* src/unrarlib.c: Remove archive name length 255 bytes restriction
	* src/unrarlib.c: Remove password length 255 bytes restriction

2006-02-28  Ying-Chun Liu  <grandpaul@gmail.com>

	* src/opts.c: Command line compatibility with non-free unrar

2004-06-14  Ben Asselstine  <ben@asselstine.com>

	* src/unrarlib.h, src/Makefile.am: Moved the _DEBUG_LOG define into 
	Makefile.am, and commented it out.
	* src/opts.c (parse_opts): fixed calling of getpass, so it only gets
	called once even if --password is on the command-line twice.
	* src/opts.c (parse_opts): now using realloc(n,NULL) to behave like
	malloc(n), so that memory allocation only happens in one place.
	* src/Makefile.in: regenerated.
	* src/unrar.c: added Jeroen's name to the visible copyright notice.

2004-06-05  Jeroen Dekkers  <jeroen@dekkers.cx>

	* src/opts.c (options): Put string constants directly in the
	structure.
